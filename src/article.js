
class Article {
    constructor(id, prix, quantiteEnStock) {
      this.id = id;
      this.prix = prix;
      this.quantiteEnStock = quantiteEnStock;
    }
    
    decrementerStock(quantite) {
      if (this.quantiteEnStock - quantite < 0) {
        throw new Error('Quantité en stock insuffisante');
      }
      this.quantiteEnStock -= quantite;
    }
    getQuantiteEnStock() {
      return this.quantiteEnStock;
    }
    incrementerStock(quantite) {
      this.quantiteEnStock += quantite;
    }
  }
  

module.exports = {
    Article:Article
}