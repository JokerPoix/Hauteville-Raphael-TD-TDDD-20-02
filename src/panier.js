class Panier {
  constructor() {
    this.articles = [];
    this.remises = {};
    this.coupons = {};
    this.remisePanier = 0;
  }

  ajouterArticle(article, quantite, date) {
    if (quantite <= 0) {
      throw new Error('La quantité doit être supérieure à 0');
    }
    this.articles.push({ article, quantite, date });
    article.decrementerStock(quantite);
  }

  appliquerCoupon(idArticle, couponCode) {
      if (!this.coupons[couponCode]) {
          throw new Error('Coupon invalide ou inexistant');
      }
      const pourcentageRemise = this.coupons[couponCode];
      const articleIndex = this.articles.findIndex(a => a.article.id === idArticle);        if (articleIndex === -1) {
          throw new Error('Article non trouvé');
        }  
      const article = this.articles[articleIndex].article;
      const nouveauPrix = article.prix - (article.prix * pourcentageRemise / 100);
      if (nouveauPrix < 0) {
          throw new Error('Le prix de l’article remisé ne peut pas être négatif');
      }
      if (this.remises[idArticle]) {
          throw new Error('Un coupon ne peut être appliqué qu’une seule fois par article');
      }
      this.remises[idArticle] = this.coupons[couponCode];
  }

  ajouterCouponValide(couponCode, montantRemise) {
      this.coupons[couponCode] = montantRemise;
  }
  appliquerRemisePanier(remise) {
      this.remisePanier = remise;
  }

  appliquerRemiseArticle(idArticle, pourcentageRemise) {
    const articleIndex = this.articles.findIndex(a => a.article.id === idArticle);
    if (articleIndex === -1) {
      throw new Error('Article non trouvé');
    }
    const article = this.articles[articleIndex].article;
    
    if (article.prix === 0 && pourcentageRemise <= 0) {
      throw new Error('Un article gratuit doit avoir une remise');
    }
  
    if (pourcentageRemise <= 0) {
      throw new Error('La remise doit être supérieure à 0');
    }
    
    this.remises[idArticle] = pourcentageRemise;
  }

  retirerArticle(idArticle) {
    const articleIndex = this.articles.findIndex(a => a.article.id === idArticle);
    if (articleIndex === -1) {
      throw new Error('Article non trouvé dans le panier');
    }
    const { article, quantite } = this.articles[articleIndex];
    this.articles.splice(articleIndex, 1);
    article.incrementerStock(quantite);
  }    

  calculerTotal() {
      let total = this.articles.reduce((acc, { article, quantite }) => {
          let prixArticle = article.prix;
          const remise = this.remises[article.id];
          if (remise) {
              prixArticle -= prixArticle * remise / 100; 
          }
          return acc + (prixArticle * quantite);
      }, 0);

      if (this.remisePanier > 0) {
          total -= total * this.remisePanier / 100;
      }

      return total;
  }
  recapitulatif() {
      let recap = 'Récapitulatif du Panier:\n';
      this.articles.forEach(({ article, quantite }) => {
        const remise = this.remises[article.id] ? this.remises[article.id] : 0;
        const prixOriginal = article.prix;
        const prixApresRemise = prixOriginal - (prixOriginal * remise / 100);
        recap += `Article ${article.id} à ${prixOriginal}€ => ${prixApresRemise.toFixed(2)}€ après ${remise}% de remise, Quantité: ${quantite}\n`;
      });
      return recap.trim();
  }
  
  viderPanier() {
    this.articles = [];
  }
}
module.exports = {
  Panier:Panier,
}