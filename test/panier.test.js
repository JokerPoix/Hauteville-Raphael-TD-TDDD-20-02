const Panier = require('../src/panier').Panier;
const Article = require('../src/article').Article;
const expect = require('chai').expect;

 describe('Panier', function () {
   let panier;
   let article;
   beforeEach(function () {
    article = new Article(1, 100, 10);
    panier = new Panier();
   });

   it('doit ajouter un article et calculer le total correctement', function () {
    panier.ajouterArticle(article, 2); 
    expect(panier.calculerTotal()).to.equal(200);
   });

   it('doit retirer un article du panier', function () {
    panier.ajouterArticle(article, 2); 
    panier.retirerArticle(article.id); 
    expect(panier.calculerTotal()).to.equal(0);
  });

   it('doit appliquer une remise sur un article spécifique', function () {
    panier.ajouterArticle(article, 1); 
    panier.appliquerRemiseArticle(article.id, 25); 
    expect(panier.calculerTotal()).to.equal(75); 
  });
  

  it('ne doit pas appliquer de remise sur les articles sans remise', function () {
    panier.ajouterArticle(article, 1);
    expect(() => panier.appliquerRemiseArticle(article.id, 0)).to.throw();
    expect(panier.calculerTotal()).to.equal(100);
  });

  it('ne doit pas permettre une remise inférieure ou égale à 0', function () {
    panier.ajouterArticle(article, 1);
    expect(() => panier.appliquerRemiseArticle(article.id, -1)).to.throw('La remise doit être supérieure à 0');
  });

  it('doit appliquer une remise seulement si le prix de l\'article est supérieur à 0 ou s\'il y a une remise', function () {
    let articleGratuit = new Article(2, 0, 10);
    panier.ajouterArticle(articleGratuit, 1);
    expect(() => panier.appliquerRemiseArticle(articleGratuit.id, 0)).to.throw('Un article gratuit doit avoir une remise');
    panier.appliquerRemiseArticle(articleGratuit.id, 20);
    expect(panier.calculerTotal()).to.equal(0);
  });

  it('doit décrémenter le stock lors de l’ajout d’un article', () => {
    panier.ajouterArticle(article, 1);
    expect(article.getQuantiteEnStock()).to.equal(9);
  });

  it('doit incrémenter le stock lors du retrait d’un article', () => {
    panier.ajouterArticle(article, 1);
    panier.retirerArticle(article.id);
    expect(article.getQuantiteEnStock()).to.equal(10);
  });

  it('ne doit pas permettre d’ajouter un article si le stock est insuffisant', () => {
    expect(() => panier.ajouterArticle(article, 11)).to.throw('Quantité en stock insuffisante');
  });

  it('doit appliquer la remise du coupon sur le total du panier', function() {
    panier.ajouterCouponValide('COUPON2024', 25); 
    panier.ajouterArticle(article, 1);
    panier.appliquerCoupon(article.id, 'COUPON2024');
    expect(panier.calculerTotal()).to.equal(75); 
  });

  it('doit appliquer une remise sur un article spécifique', function() {
    panier.ajouterArticle(article, 1);
    panier.ajouterCouponValide('COUPON2024', 25); 
    panier.appliquerCoupon(article.id, 'COUPON2024');
    expect(panier.calculerTotal()).to.equal(75);
  });

  it('ne permet pas qu’une remise s’applique plus d’une fois par article', function() {
    panier.ajouterArticle(article, 1);
    panier.ajouterCouponValide('COUPON2024', 15);
    panier.appliquerCoupon(article.id, 'COUPON2024');
    expect(() => panier.appliquerCoupon(article.id, 'COUPON2024')).to.throw('Un coupon ne peut être appliqué qu’une seule fois par article');
  });

  it('ne permet pas que le prix d’un article devienne négatif après remise', function() {
    panier.ajouterArticle(article, 1);
    panier.ajouterCouponValide('COUPON2024', 150); 
    expect(() => panier.appliquerCoupon(article.id, 'COUPON2024')).to.throw('Le prix de l’article remisé ne peut pas être négatif');
  });

  it('affiche un récapitulatif du panier avec les remises', function() {
    panier.ajouterArticle(article, 1);
    panier.ajouterCouponValide('COUPON2024', 15); 
    panier.appliquerCoupon(article.id, 'COUPON2024');
    const recap = panier.recapitulatif();
    expect(recap).to.include(`Article ${article.id} à ${article.prix}€ => 85.00€ après 15% de remise, Quantité: 1`);
  });

  it('ne doit pas permettre qu\'une remise s\'applique plus d\'une fois par article', function () {
    const article = new Article(1, 100, 10); 
    const panier = new Panier(); 
    panier.ajouterArticle(article, 1); 
    panier.ajouterCouponValide('COUPONTEST', 15); 
    panier.appliquerCoupon(1, 'COUPONTEST'); 
    expect(() => panier.appliquerCoupon(1, 'COUPONTEST')).to.throw('Un coupon ne peut être appliqué qu’une seule fois par article'); // Tente d'appliquer à nouveau le coupon et s'attend à une erreur
});
  
 });
